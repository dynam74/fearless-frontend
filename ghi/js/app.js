function createCard(name, description, pictureUrl, start, end, location) {
  return `
      <div class="col">
    <div class="card shadow mb-3 bg-body-tertiary rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
      ${start} - ${end}
      </div>
    </div>

  `;
}


function createPlaceHolder() {
  return `
  <div class="card" aria-hidden="true" id="placeholder">
  <img src="..." class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title placeholder-glow">
      <span class="placeholder col-6"></span>
    </h5>
    <p class="card-text placeholder-glow">
      <span class="placeholder col-7"></span>
      <span class="placeholder col-4"></span>
      <span class="placeholder col-4"></span>
      <span class="placeholder col-6"></span>
      <span class="placeholder col-8"></span>
    </p>
    <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
  </div>
</div>
  `;
}



window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      throw new Error('Response not ok');
    } else {
      const data = await response.json();

      for(let i = 0; i < data.conferences.length; i++) {
          const column = document.querySelector('#rows');
          let placeHolderhtml = createPlaceHolder()
          column.innerHTML += placeHolderhtml
      }

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts);
          const start = `${startDate.getMonth()+1}/${startDate.getDate()}/${startDate.getFullYear()}`;
          const endDate = new Date(details.conference.ends);
          const end = `${endDate.getMonth()+1}/${endDate.getDate()}/${endDate.getFullYear()}`;
          const location = details.conference.location.name;


          const html = createCard(name, description, pictureUrl, start, end, location);
          const column = document.querySelector('#rows');
          var placeHolder = document.querySelector('#placeholder')
          placeHolder.remove()
          column.innerHTML += html;

        }
      }

    }
  } catch (e) {
    // Figure out what to do if an error is raised
    console.error('error', e)
  }

});

